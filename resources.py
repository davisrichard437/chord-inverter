# resources.py
# provides list of resources (vars and functions) for chord inversion

"""List of resources for chord inversion."""

# Note: currently unused, try to implement later?
# from typing import Annotated
# Note = Annotated[int, "pitch class integer of note"]
# Chord = Annotated[tuple[Note, ...], "list of pitch class integers"]
# Chords = Annotated[list[Chord], "list of chords"]

Note = int
Interval = int
Chord = tuple[Note, ...]
Chords = list[Chord]

default_notes = {0:  "c",
                 1:  "cs",
                 2:  "d",
                 3:  "ef",
                 4:  "e",
                 5:  "f",
                 6:  "fs",
                 7:  "g",
                 8:  "af",
                 9:  "a",
                 10: "bf",
                 11: "b"}

# Used in `output_to_lilypond' to determine which tritones to apply octave displacement
default_notes_augmented_fourths = [[0, 6],
                                   [3, 9],
                                   [5, 11],
                                   [7, 1],
                                   [8, 2],
                                   [10, 4]]

note_names = {"c": 0, "bs": 0,
              "cs": 1, "df": 1,
              "d": 2,
              "ds": 3, "ef": 3,
              "e": 4, "ff": 4,
              "f": 5, "es": 5,
              "fs": 6, "gf": 6,
              "g": 7,
              "gs": 8, "af": 8,
              "a": 9,
              "as": 10, "bf": 10,
              "b": 11, "cf": 11}

valid_notes = ["c", "bs", "cs", "df", "d", "ds", "ef",
               "e", "ff", "f", "es", "fs", "gf", "g",
               "gs", "af", "a", "as", "bf", "b", "cf"]


def is_tool(name: str = "") -> bool:
    """Check whether `name` is on PATH and marked as executable."""
    from shutil import which
    return which(name) is not None


def remove_duplicates(chords: Chords) -> Chords:
    """Removes duplicates from list."""
    return list(dict.fromkeys(chords))


def get_note(prompt: str = "Enter note name: ") -> Note:
    """Gets user input for note and returns pitch class integer."""
    while True:
        note = str(input(prompt))
        if note in valid_notes:
            note_int: int = note_names[note]
            return note_int
        else:
            print("Invalid note name.")


def get_notes(number: int = 1) -> list[Note]:
    """Runs get_note() number times and returns list of pitch class integers."""
    chord: list[int] = []
    for i in range(1, int(number)+1):
        p = "Enter name of note " + str(i) + ": "
        chord.append(get_note(prompt=p))
    return chord


def get_interval(prompt: str = "Enter interval in semitones: ") -> Note:
    """Gets user input for interval and returns number of semitones."""
    while True:
        interval = input(prompt)
        if interval.isdigit() is True:
            if int(interval) > 0:
                int_interval: int = int(interval)
                return int_interval
            else:
                print("Invalid interval. Please enter an interger greater than 1.")
        else:
            print("Invalid input. Please enter an interger greater than 1.")


def check_valid_notes(chord: list[int] = []) -> bool:   # Unused
    """Checks chord (list) for valid notes."""
    for i in chord:
        if i in valid_notes:
            continue
        else:
            return False
    return True


def get_internal_interval(low_note: Note = 0, high_note: Note = 0) -> Interval:
    """Calculates smallest internal interval (in semitones) between pair of notes."""
    internal_interval: Interval = 0
    if high_note > low_note:
        internal_interval = high_note - low_note
        return internal_interval
    elif high_note <= low_note:
        internal_interval = high_note + 12 - low_note
        return internal_interval
    else:
        return 0


def get_outer_interval(chord: Chord = ()) -> Interval:
    """Gets smallest outer interval (in semitones) for a chord."""
    total_interval: int = 0
    for i in range(0, len(chord)-1):
        total_interval += get_internal_interval(chord[i], chord[i+1])
    return total_interval


def get_internal_intervals(chord: Chord) -> list[Interval]:
    """Get list of internal intervals of chord."""
    internal_intervals = []
    for i in range(len(chord)-1):
        internal_intervals.append(get_internal_interval(chord[i], chord[i+1]))
    return internal_intervals


def invert_in_place(chord: Chord = ()) -> Chord:
    """Inverts chord, preserving outer intervals."""
    output_chord = [chord[0]]   # initialize list with lowest note of chord
    intervals = get_internal_intervals(chord)
    intervals.reverse()
    for interval in intervals:
        new_note = (output_chord[-1] + interval) % 12
        output_chord.append(new_note)
    return tuple(output_chord)


def print_results(chord: list[Note] = [],
                  chords: Chords = [],
                  filters: list[str] = []):
    """Output chords to stdout."""
    output_chord = []
    for n in chord:
        output_chord.append(default_notes[n])
    print("Original: " + ','.join(output_chord))
    for f in filters:
        print(f)
    for c in chords:
        print_chord = []
        for n in c:
            print_chord.append(default_notes[n])
        print(','.join(print_chord))


def increment_filename() -> int:
    """Gets filename increment number for new output."""
    import os
    i = 0
    while (
            os.path.exists("chord-inversions_%s.txt" % i) or
            os.path.exists("chord-inversions_%s.ly" % i)
    ):
        i += 1
    return i


def output_to_text(chord: list[int] = [],
                   chords: Chords = [],
                   file_number: int = 0,
                   filters: list[str] = []):
    """Output chords to human-readable file."""
    print("Writing inversions to 'chord-inversions_%s.txt'." % file_number)
    original_chord = []
    for n in chord:
        original_chord.append(default_notes[n])
    file_name = "chord-inversions_" + str(file_number) + ".txt"
    with open(file_name, "w") as output_file:
        print("Original: " + ','.join(original_chord), file=output_file)
        for f in filters:
            print(f, file=output_file)
        for c in chords:
            print_chord = []
            for n in c:
                print_chord.append(default_notes[n])
            print(','.join(print_chord), file=output_file)


def output_to_lilypond(chords: Chords = [], file_number: int = 0):
    """Output chords to lilypond file."""
    import re, subprocess
    if not is_tool("lilypond"):
        print("Lilypond not found.")
        return None
    file_name = f"chord-inversions_{file_number}.ly"
    lilypond_version = re.split(r' |\\s|\\n', str(subprocess.check_output(["lilypond", "--version"])))[2]
    print(f"Writing inversions to '{file_name}'")
    with open(file_name, "w") as output_file:
        print("\\version \"" + lilypond_version + "\"", file=output_file)
        print("\\language \"english\"", file=output_file)
        print("\\score{", file=output_file)
        print("  \\relative c' {", file=output_file)
        for c in chords:
            print_chord = ["    <"]
            for i, n in enumerate(c):
                if i > 0:
                    if (
                            get_internal_interval(c[i-1], n) >= 6 and
                            [c[i-1], n] not in default_notes_augmented_fourths
                    ):
                        print_chord.append(default_notes[n] + "'")
                    else:
                        print_chord.append(default_notes[n])
                else:
                    print_chord.append(default_notes[n])
            print_chord.append('>1')
            print(' '.join(print_chord), file=output_file)
        print("  }", file=output_file)
        print("  \\layout{}", file=output_file)
        print("  \\midi{}", file=output_file)
        print("}", file=output_file)
    subprocess.run(["lilypond", file_name])
