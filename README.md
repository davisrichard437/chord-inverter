# Chord Inverter

Simple python script to invert and filter chords and store output. Optionally uses [lilypond](http://lilypond.org/) to output files to `.pdf` and `.midi` files.

## Dependencies

- Python 3.9 or later
- Lilypond (optional)

## Usage

Clone the repository:

```
git clone https://gitlab.com/davisrichard437/chord-inverter.git
cd chord-inverter
```

Run the chord inverter interactively:

    ./chord-inverter.py --interactive

Or with command line arguments, for example:

    ./chord-inverter.py --notes a b c --lownote c --text --lilypond

See `./chord-inverter.py --help` for more information.

## Program Structure

The script uses the following naming conventions:

- `note` (int): a pitch class integer (from 0-11) representing an individual note. Sometimes abbreviated `n`.
- `interval` (int): an integer representing the number of semitones between two notes.
- `chord` (tuple(int, ...)): a tuple of notes representing an individual chord, in ascending pitch order. Sometimes abbreviated `c`.
- `chords` (list(tuple(int, ...))): a list of chords representing a set of inversions of a chord.

The script depends on the following files for their respective functionality:

- `chord-inverter.py` (script): the "main" file of the script
  - Gets initial chord from user
  - Iterates through all classes in `filters.py`
    - Asks user to apply filter
    - Gets user input for appropriate parameter
    - Filters chords accordingly
  - Asks user to output to text file
  - Asks user to output to lilypond (and `.pdf`, `.midi`) file
- `filters.py` (module): collection of filters used to narrow list of chords. Filters are classes with the following structure:
  - `human_readable_parameter` (method): returns a human-readable version of the user parameter of the filter (e.g. the note 10 would return bf), used to label filtered chords
  - `__init__` (method): defines the following attributes:
    - `filter_name`: (attribute, string): the name of the filter
    - `user_parameters` (attribute, dictionary): formatted `{"parameter name": parameter_method}`, lists user parameters for filter and references a class method to get them
  - `do_filter` (method): narrows list of chords according to the filter defined by the class and its relevant parameters
- `piano.py` (module): list of resources for determining if chords are piano-playable
- `resources.py` (module): general list of resources for operating on chords, notes, intervals, output files, etc.
