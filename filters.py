# filters.py
# Provides list of filters for use by chord-inverter.py

from typing import Callable
import resources
from resources import Note, Interval, Chord, Chords

class FilterTemplate:
    """A template to be inherited by subsequent filter classes."""

    def get_parameter(
            self,
            value,
            interactive: bool,
            get_interactively: Callable,
            get_programmatically: Callable
    ):
        if interactive:
            return get_interactively()
        else:
            return get_programmatically(value)

    def __init__(
            self,
            filter_name: str,
            unit: str,
            parameter_type: type,
            user_parameter: Callable
    ) -> None:
        self.filter_name = filter_name
        self.unit = unit
        self.parameter_type = parameter_type
        self.user_parameters = {self.filter_name: user_parameter}

    def do_filter(self, lam: Callable, chords: Chords) -> Chords:
        return resources.remove_duplicates(list(filter(lam, chords)))


class LowNoteFilter(FilterTemplate):
    """Filter by the low note of the chord."""

    def get_low_note(self, value: str = "c", interactive: bool = False) -> Note:
        """Get lowest note of chord from user and return pitch class integer."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_note(prompt="Enter name of lowest note of chord: "),
            lambda x : resources.note_names[x]
        )

    def human_readable_parameter(self, note: Note = 0) -> str:
        """Return human readable lowest note."""
        return resources.default_notes[note]

    def __init__(self):
        super().__init__("low note", "pitch", str, self.get_low_note)

    def do_filter(
            self,
            note: Note = 0,
            chords: Chords = []
    ) -> list[tuple[int, ...]]:
        """Check list of chords against target low note and return filtered list."""
        return super().do_filter(lambda chord : chord[0] == note, chords)


class HighNoteFilter(FilterTemplate):
    """Filter by the high note of the chord."""

    def get_high_note(self, value: str = "c", interactive: bool = False) -> Note:
        """Gets highest note of chord from user, returns pitch class integer."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_note(prompt="Enter name of highest note of chord: "),
            lambda x : resources.note_names[x]
        )

    def human_readable_parameter(self, note: Note = 0) -> str:
        """Return human readable highest note."""
        return resources.default_notes[note]

    def __init__(self):
        super().__init__("high note", "pitch", str, self.get_high_note)

    def do_filter(self,
                  note: Note = 0,
                  chords: Chords = []) -> Chords:
        """Check list of chords against target high note and return filtered list."""
        return super().do_filter(lambda chord : chord[len(chord)-1] == note, chords)


class MaximumOuterIntervalFilter(FilterTemplate):
    """Filter by maximum outer interval of chord."""

    def get_max_outer_interval(self, value: int = 0, interactive = False) -> Interval:
        """Get maximum outer interval from user and return number of semitones."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_interval(prompt="Enter maximum outer interval in semitones: "),
            lambda value : value
        )

    def __init__(self):
        super().__init__("maximum outer interval", "semitones", int, self.get_max_outer_interval)

    def human_readable_parameter(self, interval: Interval = 0) -> str:
        """Return human readable interval."""
        return f"{interval} {self.unit}"

    def do_filter(self,
                  interval: Interval = 0,
                  chords: Chords = []) -> Chords:
        """Check list of chords against maximum outer interval and return filtered list."""
        return super().do_filter(lambda chord : resources.get_outer_interval(chord) <= interval, chords)


class MinimumOuterIntervalFilter(FilterTemplate):
    """Filter by minimum outer interval of chord."""

    def get_min_outer_interval(self, value: Interval = 0, interactive = False) -> Interval:
        """Get minimum outer interval and return number of semitones."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_interval(prompt="Enter minimum outer interval in semitones: "),
            lambda value : value
        )

    def __init__(self):
        super().__init__("minimum outer interval", "semitones", int, self.get_min_outer_interval)

    def human_readable_parameter(self, interval: Interval = 0) -> str:
        """Return human readable highest note."""
        return f"{interval} {self.unit}"

    def do_filter(self,
                  interval: Interval = 0,
                  chords: Chords = []) -> Chords:
        """Check list of chords against minimum outer interval and return filtered list."""
        return super().do_filter(lambda chord : resources.get_outer_interval(chord) >= interval, chords)


class ExactOuterIntervalFilter(FilterTemplate):
    """Filter by exact outer interval of chord."""

    def get_exact_outer_interval(self, value: Interval = 0, interactive = False) -> int:
        """Get outer interval from user and return number of semitones."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_interval(prompt="Enter exact outer interval in semitones: "),
            lambda value : value
        )

    def __init__(self):
        super().__init__("exact outer interval", "semitones", int, self.get_exact_outer_interval)

    def human_readable_parameter(self, interval: int = 0) -> str:
        """Return human readable interval."""
        return f"{interval} {self.unit}"

    def do_filter(self,
                  interval: int = 0,
                  chords: Chords = []) -> Chords:
        """Check list of chords against exact outer interval and return filtered list."""
        return super().do_filter(lambda chord : resources.get_outer_interval(chord) == interval, chords)


class MaximumInternalIntervalFilter(FilterTemplate):
    """Filter by maximum internal interval within a chord."""

    def get_max_internal_interval(self, value: int = 0, interactive = False) -> int:
        """Get maximum internal interval from user and return number of semitones."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_interval(prompt="Enter maximum internal interval in semitones: "),
            lambda value : value
        )

    def __init__(self):
        super().__init__("maximum internal interval", "semitones", int, self.get_max_internal_interval)

    def human_readable_parameter(self, i: int = 0) -> str:
        """Return human readable interval."""
        return f"{i} {self.unit}"

    def check_internal_interval(self, chord: tuple[int, ...], interval: int = 0) -> bool:
        """Check if all internal intervals of chord complie with filter."""
        for i in range(0, len(chord)-1):
            if resources.get_internal_interval(chord[i], chord[i+1]) > interval:
                return False
        return True

    def do_filter(self,
                  interval: int = 0,
                  chords: Chords = []) -> Chords:
        """Check list of chords against maximum internal interval and return filtered list."""
        return super().do_filter(lambda chord : self.check_internal_interval(chord, interval), chords)


class MinimumInternalIntervalFilter(FilterTemplate):
    """Filter by minimum internal interval within a chord."""

    def get_min_internal_interval(self, value: int = 0, interactive = False) -> int:
        """Get minimum internal interval from user and return number of semitones."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_interval(prompt="Enter minimum internal interval in semitones: "),
            lambda value : value
        )

    def __init__(self):
        super().__init__("minimum internal interval", "semitones", int, self.get_min_internal_interval)

    def human_readable_parameter(self, i: int = 0) -> str:
        """Return human readable interval."""
        return f"{i} {self.unit}"

    def check_internal_interval(self, chord: tuple[int, ...], interval: int = 0) -> bool:
        """Check if all internal intervals of chord complie with filter."""
        for i in range(0, len(chord)-1):
            if resources.get_internal_interval(chord[i], chord[i+1]) < interval:
                return False
        return True

    def do_filter(self,
                  interval: int = 0,
                  chords: Chords = []) -> Chords:
        """Check list of chords against minimum internal interval and return filtered list."""
        return super().do_filter(lambda chord : self.check_internal_interval(chord, interval), chords)


class PianoPlayableLeftHandFilter(FilterTemplate):
    """Filter if chords playable by left hand on piano."""

    def get_max_stretch(self, value: Interval = 0, interactive = False) -> Interval:
        """Get maximum hand stretch and return number of semitones."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_interval(prompt="Enter maximum hand stretch: "),
            lambda value : value
        )

    def __init__(self):
        super().__init__("piano playable left hand", "semitones", int, self.get_max_stretch)

    def human_readable_parameter(self, i: Interval = 12) -> str:
        """Return human readable interval."""
        return f"{i} {self.unit}"

    def check_playable(self, chord: Chord, stretch: Interval = 12) -> bool:
        """Check chord and return true if playable."""
        import piano
        match len(chord):
            case 1:
                return piano.dyad_playable(chord, stretch=stretch)
            case 2:
                return piano.trichord_playable_LH(chord, stretch=stretch)
            case 3:
                return piano.trichord_playable_LH(chord, stretch=stretch)
            case 4:
                # In progress!
                return piano.tetrachord_playable_LH(chord, stretch=stretch)
            case 5:
                # In progress!
                return piano.pentachord_playable_LH(chord, stretch=stretch)

    def do_filter(self,
                  stretch: Interval = 12,
                  chords: Chords = []) -> Chords:
        """Checks list of chords for left hand piano playability."""
        return super().do_filter(lambda chord : self.check_playable(chord, stretch=stretch), chords)


class PianoPlayableRightHandFilter(FilterTemplate):
    """Filter if chords playable by right hand on piano."""

    def get_max_stretch(self, value: Interval = 0, interactive = False) -> Interval:
        """Get maximum hand stretch and return number of semitones."""
        return super().get_parameter(
            value,
            interactive,
            lambda : resources.get_interval(prompt="Enter maximum hand stretch: "),
            lambda value : value
        )

    def __init__(self):
        super().__init__("piano playable right hand", "semitones", int, self.get_max_stretch)

    def human_readable_parameter(self, interval: Interval = 12) -> str:
        """Return human readable interval."""
        return f"{interval} {self.unit}"

    def check_playable(self, chord: Chord, stretch: Interval = 12) -> bool:
        """Check chord and return true if playable."""
        import piano
        match len(chord):
            case 1:
                return piano.dyad_playable(chord, stretch=stretch)
            case 2:
                return piano.trichord_playable_RH(chord, stretch=stretch)
            case 3:
                return piano.trichord_playable_RH(chord, stretch=stretch)
            case 4:
                # In progress!
                return piano.tetrachord_playable_RH(chord, stretch=stretch)
            case 5:
                # In progress!
                return piano.pentachord_playable_RH(chord, stretch=stretch)

    def do_filter(self,
                  stretch: int = 12,
                  chords: Chords = []) -> Chords:
        """Checks list of chords for right hand piano playability."""
        return super().do_filter(lambda chord : self.check_playable(chord, stretch=stretch), chords)
