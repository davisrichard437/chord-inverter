#!/usr/bin/env bash
# script to test the chord inverter script

echo "Help text"
./chord-inverter.py --help
echo "All inversions of 'bf,d,e,fs'"
./chord-inverter.py --notes bf d e fs
echo "'b,d,e,fs' filtered by low note d"
./chord-inverter.py --notes bf d e fs\
                    --low-note d
echo "'b,d,e,fs' filtered by exact outer interval 8 semitones"
./chord-inverter.py --notes bf d e fs\
                    --exact-outer-interval 8
echo "'b,d,e,fs' filtered by high note d"
./chord-inverter.py --notes bf d e fs\
                    --high-note d
echo "'b,d,e,fs' filtered by maximum interval 8 semitones"
./chord-inverter.py --notes bf d e fs\
                    --maximum-internal-interval 8
echo "'b,d,e,fs' filtered by maximum outer interval 8 semitones"
./chord-inverter.py --notes bf d e fs\
                    --maximum-outer-interval 8
echo "'b,d,e,fs' filtered by minimum internal interval 8 semitones"
./chord-inverter.py --notes bf d e fs\
                    --minimum-internal-interval 8
echo "'b,d,e,fs' filtered by minimum outer interval 8 semitones"
./chord-inverter.py --notes bf d e fs\
                    --minimum-outer-interval 8

                    # --text\
                    # --lilypond
