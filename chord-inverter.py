#!/usr/bin/env python3
# chord-inverter.py
# Provides all inversions of a given chord according to a list of filters

from itertools import permutations
import inspect
import filters
from filters import *
import resources
import argparse

##########################
# Command line arguments #
##########################

parser = argparse.ArgumentParser(description="Invert chords and filter results.")

parser.add_argument(
    '-n',
    '--notes',
    metavar='N',
    type=str,
    nargs='+',
    help="A list of English lilypond notes separated by spaces."
)

parser.add_argument(
    '-i',
    '--interactive',
    action='store_true',
    help="Run the script interactively."
)

parser.add_argument(
    '-t',
    '--text',
    action='store_true',
    help="Output to text."
)

parser.add_argument(
    '-l',
    '--lilypond',
    action='store_true',
    help="Output to lilypond."
)

#######################################
# Get list of filters from filters.py #
#######################################

filters_list = []

def populate_filters_list():
    """Scan filters module for appropriate filter classes, adding them to `filters_list` and and command line arguments."""
    for name, obj in inspect.getmembers(filters, inspect.isclass):
        if (                    # Remove unneeded classes
                name != "FilterTemplate" and
                name != "Note" and
                name != "Interval" and
                name != "Chord" and
                name != "Chords"
        ):
            f = obj()
            # Add all filter classes to `filters_list`, to later be iterated over
            filters_list.append(f)
            # Add parser argument for every filter class
            parser.add_argument(
                f"--{f.filter_name.replace(' ', '-')}".lower(),
                metavar=f.unit,
                type=f.parameter_type,
                help=f"Filter by {f.filter_name}."
            )

populate_filters_list()

args = parser.parse_args()

##################
# Run the script #
##################

def get_number_notes_interactively() -> int:
    """Interactively get number of notes in the chord from user."""
    valid_number = False
    while not valid_number:
        num_notes = input("How many notes in the chord? ")
        if num_notes.isdigit() is False:  # Check if input is a number
            print("Input not a valid number, please try again.")
        elif int(num_notes) < 1:          # Check if input number is valid
            print("Please enter a number greater than 0.")
        else:
            valid_number = True
            return int(num_notes)

def get_notes_interactively(n: int) -> list[int]:
    """Interactively get notes from user."""
    print("Use lilypond english note names.")
    c = resources.get_notes(n)
    return c

# list of strings "parameter_name: parameter_value"
# to pass to resources.output_to_text()
parameter_values = []

def get_filters_interactively(inversions: Chords) -> Chords:
    """Interactively apply user-specified filters."""
    for f in filters_list:
        parameters = [] # list of parameters to pass to `do_filter`
        if str(input(f"Filter by {f.filter_name}? [y/N] ")).lower() == "y":
            for p in f.user_parameters:
                # Get value of parameter for filter
                current_parameter = f.user_parameters[p](interactive = True)
                parameters.append(current_parameter)
                # Append parameter and filter for output
                parameter_value = f"{p}: {f.human_readable_parameter(current_parameter)}"
                parameter_values.append(parameter_value)
            parameters.append(inversions)
            inversions = f.do_filter(*parameters)
    return inversions

def print_results_interactively(chord: list[int], inversions: Chords):
    """Print results to stdout, text, and lilypond, according to user input."""
    resources.print_results(chord, inversions, parameter_values) # stdout
    file_number = resources.increment_filename()
    # Print to text and Lilypond
    if str(input("Output to text file? [y/N] ")).lower() == "y":
        resources.output_to_text(
            chord,
            inversions,
            file_number=file_number,
            filters=parameter_values
        )
    if str(input("Output to lilypond file? [y/N] ")).lower() == "y":
        resources.output_to_lilypond(inversions, file_number)

def run_interactively():
    """Run the script interactively."""
    try:
        num_notes = get_number_notes_interactively()
        chord = get_notes_interactively(num_notes)
        inversions = get_filters_interactively(list(permutations(chord)))
        final_inversions = resources.remove_duplicates(inversions)
        print_results_interactively(chord, final_inversions)
    except KeyboardInterrupt:
        print("\nCtrl-c (keyboard interrupt) pressed, exiting.")
        quit()

def get_filters_programmatically(inversions: Chords) -> Chords:
    for f in filters_list:
        parameters = [] # list of parameters to pass to `do_filter`
        # get value of current parameter from command line argument
        current_parameter = vars(args)[f"{f.filter_name.replace(' ', '_')}"]
        if current_parameter is not None:
            for p in f.user_parameters:
                current_parameter = f.user_parameters[p](value = current_parameter)
                parameters.append(current_parameter)
                parameter_value = f"{p}: {f.human_readable_parameter(current_parameter)}"
                parameter_values.append(parameter_value)
            parameters.append(inversions)
            inversions = f.do_filter(*parameters)
    return inversions

def print_results_programatically(chord: list[int], inversions: Chords):
    """Print to stdout, text, and lilypond according to command line arguments."""
    resources.print_results(chord, inversions, parameter_values) # stdout
    file_number = resources.increment_filename()
    # Print to text and lilypond
    if args.text:
        resources.output_to_text(
            chord,
            inversions,
            file_number=file_number,
            filters=parameter_values
        )
    if args.lilypond:
        resources.output_to_lilypond(inversions, file_number)

def run_programmatically():
    """Run the script programmatically, with command-line arguments."""
    chord = [resources.note_names[note] for note in args.notes]
    inversions = get_filters_programmatically(list(permutations(chord)))
    print_results_programatically(chord, inversions)

if args.interactive:
    run_interactively()
else:
    run_programmatically()
