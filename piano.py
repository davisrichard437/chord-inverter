# piano.py
# Provides list of resources to determine if chord is piano-playable

import resources

white_notes = [0, 2, 4, 5, 7, 9, 11]
black_notes = [1, 3, 6, 8, 10]

def dyad_playable(chord: tuple[int, int], stretch: int = 12) -> bool:
    """Return True if dyad is within maximum stretch."""
    return resources.get_internal_interval(chord[0], chord[1]) <= stretch


def trichord_playable_RH(chord: tuple[int, int, int], stretch: int = 12) -> bool:
    """Return True if trichord is playable by right hand."""
    if (
            # Check if greater than maximum stretch
            resources.get_outer_interval(chord) > stretch or (
                # Check if less than m3 between thumb and forefinger
                # and within 2 semitones below maximum stretch
                resources.get_internal_interval(chord[0], chord[2]) > stretch - 2 and
                resources.get_internal_interval(chord[0], chord[1]) < 4
            ) or (
                # Check if less than M2 between thumb and forefinger
                # and within 5 semitones below maximum stretch
                resources.get_internal_interval(chord[0], chord[2]) > stretch - 5 and
                resources.get_internal_interval(chord[0], chord[1]) < 2
            )
    ):
        return False
    else:
        return True


def trichord_playable_LH(chord: tuple[int, int, int], stretch: int = 12) -> bool:
    """Return True if trichord is playable by left hand."""
    return trichord_playable_RH(resources.invert_in_place(chord), stretch)


def tetrachord_playable_LH(chord: tuple[int, int, int, int], stretch: int = 12) -> bool:
    """Return True if tetrachord is playable by left hand."""
    outer_interval = resources.get_outer_interval(chord)
    interval_0_1 = resources.get_internal_interval(chord[0], chord[1])
    interval_1_2 = resources.get_internal_interval(chord[1], chord[2])
    interval_2_3 = resources.get_internal_interval(chord[2], chord[3])
    if (
            # Check if greater than maximum stretch
            outer_interval > stretch or (
                # Check for playability at max stretch
                outer_interval == stretch and (
                    interval_2_3 <= 5 or (
                        interval_2_3 == 5 and (
                            interval_1_2 < 2 or
                            interval_0_1 < 2
                        )
                    )
                )
            ) or (
                # Check for playability at max stretch - 1
                resources.get_outer_interval(chord) == stretch - 1
            ) or (
                # Check for playability at max stretch - 2
                resources.get_outer_interval(chord) == stretch - 2
            ) or (
                # Check for playability at max stretch - 3
                resources.get_outer_interval(chord) == stretch - 3
            ) or (
                # Check for playability at max stretch - 4
                resources.get_outer_interval(chord) == stretch - 4
            ) or (
                # Check for playability at max stretch - 5
                resources.get_outer_interval(chord) == stretch - 5
            )
            # More here
    ):
        return False
    else:
        return True


def tetrachord_playable_RH(chord: tuple[int, int, int, int], stretch: int = 12) -> bool:
    """Return True if tetrachord is playable by left hand."""
    return tetrachord_playable_LH(resources.invert_in_place(chord), stretch)


def pentachord_playable_LH(chord: tuple[int, int, int, int, int], stretch: int = 12) -> bool:
    """Return True if tetrachord is playable by left hand."""
    if (
            # Check if greater than maximum stretch
            resources.get_outer_interval(chord) > stretch
            # More here
    ):
        return False
    else:
        return True


def pentachord_playable_RH(chord: tuple[int, int, int, int, int], stretch: int = 12) -> bool:
    """Return True if tetrachord is playable by left hand."""
    return pentachord_playable_LH(resources.invert_in_place(chord), stretch)


def interval_LH_5_2(chord: tuple[int, ...], forefinger_index: int = 1) -> bool:
    """Return True if chord does not have unsuitable interval between pinky and forefinger."""
    if resources.get_internal_interval(chord[0], chord[forefinger_index]) == 11:
        if (                # False if M7 between 5 and 2 on opposite color keys
                (
                    chord[0] in white_notes and
                    chord[forefinger_index] in black_notes
                ) or
                (
                    chord[0] in black_notes and
                    chord[forefinger_index] in white_notes
                )
        ):
            return False
        else:
            return True
    elif resources.get_internal_interval(chord[0], chord[forefinger_index]) == 10:
        if (                # False if m7 on white keys and top key is black
                chord[0] in white_notes and
                chord[forefinger_index] in white_notes and
                chord[len(chord)-1] in black_notes
        ):
            return False
        else:
            return True
    else:
        return True

def interval_LH_5_3(chord: tuple[int, ...], middle_index: int = 1) -> bool:
    """Return True if chord does not have unsuitable interval between pinky and middle finger."""
